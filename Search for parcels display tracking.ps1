# Search through xml tracking files sent to Amazon for parcels statuses.
#
# By Frazer Grant date 28/02/2018 

# Version 1.0

# Version 1.1 - add  link to the tracking files, so that someone can download the tracking file and look at it.  They will have to copy and paste the link into a browser or file explorer but they will have to enter their WNDCloud credentails to download it.

# Version 1.2 - added 2 links one for Mac and one for PC.  Had to create a share on CT, created AD secuirty group  added IT support, Nick and Seema to it.

# Version 1.3 - Massive performance boost.  Now read in each file into memory then scan for parcels.  Instead of checking each  file for a string match.  Checking 800 parcels in 700 files used to take 26 minutes, now it take 80 seconds.

# Version 1.4 - added file size in KB, added date/time stamp to results file, amended Mac link.

# Version 1.5 - Added country code. - next version search between a time frame

# Version 1.6 - Added variable $heavyLoad, set to 1 finds the files on CT and then copy them to local PC.  Set to 0 runs a command on CT to find the files, zip them and then copy to local PC

# useful note:
# using regex to search through files for variables, however not useful for this solution.
# Select-String -Pattern "\b(AW000071097|AW000072199)W+(?:\w+\W+){1,10}?K1\b"

# If local PC is on a different heavy load. 1 will copy each file to local PC or if CT is under heavy load.  0 will zip the files and then copy, to help speed things up, only if CT is idle, otherwise it will strain CT.
$heavyLoad = 1


# type in the parcel number use | to seperate the parcels
#$parcels = "AW000317138|AW000322751|AW000323129|AW000318956|AW000322638|AW000317179|AW000325543|AW000316239|AW000322280|AW000321565|AW000322726|AW000322728|AW000322706|AW000321138|AW000320929|AW000324564|AW000322120|AW000321008|AW000321941|AW000325127|AW000322328|AW000320004|AW000321841|AW000323095|AW000322522|AW000321960|AW000322688|AW000317210|AW000322699|AW000320822|AW000321419|AW000322527|AW000322234|AW000322606|AW000317714|AW000321919|AW000324839|AW000322417|AW000320915|AW000321513|AW000322512|AW000325209|AW000322868|AW000321473|AW000322901|AW000321424|AW000323051|AW000317535|AW000323077|AW000319053|AW000322261|AW000322674|AW000324478|AW000323035|AW000317017|AW000322398|AW000322132|AW000317357|AW000322750|AW000322555|AW000322426|AW000320586|AW000321361|AW000320830|AW000321412|AW000322057|AW000321040|AW000320440|AW000324335|AW000322112|AW000320360|AW000316310|AW000322134|AW000320987|AW000320530|AW000322530|AW000321297|AW000322108|AW000321083|AW000323127|AW000322689|AW000322481|AW000322298|AW000323241|AW000320410|AW000321061|AW000323013|AW000317800|AW000324660|AW000322698|AW000323041|AW000322770|AW000322262|AW000320542|AW000324548|AW000316314|AW000320804|AW000324571|AW000316985|AW000322465|AW000309176|AW000297361|AW000320883|AW000322972|AW000294233|AW000314909|AW000316081|AW000312825|AW000318238|AW000288673|AW000312416|AW000313136|AW000315001|AW000320064|AW000317153|AW000314203|AW000296542|AW000312696|AW000316081|AW000316139|AW000318695|AW000313863|AW000286699|AW000304988|AW000301986|AW000322033|AW000324755|AW000314883|AW000314739|AW000322291|AW000313059|AW000310324|AW000314361|AW000311638|AW000311866|AW000310138|AW000312480|AW000310582|AW000311500|AW000313577|AW000314241|AW000320207|AW000314448|AW000322551|AW000312099|AW000305584|AW000305579|AW000317525|AW000309350|AW000321570|AW000323251|AW000319721|AW000307882|AW000319006|AW000305716|AW000321786|AW000314654|AW000305628|AW000315118|AW000319497|AW000317742|AW000314429|AW000317569|AW000286498|AW000316441|AW000315916|AW000317348|AW000314029|AW000281403|AW000313088|AW000317496|AW000298445|AW000316807|AW000312864|AW000309868|AW000305467|AW000314541|AW000312563|AW000315284|AW000319637|AW000311916|AW000321939|AW000315867|AW000315071|AW000285182|AW000316812|AW000312568|AW000319775|AW000312410|AW000315060|AW000324980|AW000324762|AW000324180|AW000326826|AW000323953|AW000318952|AW000326015|AW000326953|AW000322175|AW000326901|AW000324911|AW000324160|AW000326177|AW000326466|AW000325646|AW000324035|AW000324140|AW000327613|AW000325939|AW000325572|AW000327227|AW000325960|AW000323161|AW000325187|AW000327745|AW000324112|AW000328772|AW000318832|AW000326866|AW000328842|AW000327421|AW000324800|AW000326351|AW000309847|AW000296010|AW000314380|AW000313128|AW000314444|AW000309838|AW000310725|AW000314596|AW000318535|AW000311354|AW000314060|AW000315125|AW000321044|AW000323740|AW000315502|AW000309719|AW000315592|AW000315302|AW000317538|AW000312955|AW000318913|AW000321726|AW000313684|AW000305477|AW000313404|AW000316494|AW000312944|AW000316563|AW000321117|AW000320376|AW000313269|AW000317799|AW000310592|AW000305874|AW000318778|AW000318307|AW000324755|AW000317799|AW000315155|AW000316904|AW000318647|AW000314201|AW000322965|AW000321578|AW000320284|AW000316301|AW000316451|AW000313253|AW000314508|AW000323276|AW000304470|AW000319672|AW000307622|AW000314267|AW000312322|AW000311641|AW000309719|AW000314626|AW000308766|AW000320307|AW000317907|AW000308002|AW000318868|AW000316493|AW000309057|AW000314323|AW000316094|AW000312770|AW000309774|AW000309156|AW000317907|AW000315980|AW000317396|AW000315565|AW000320639|AW000318313|AW000309806"
$parcels = "AW000353971|AW000353863|AW000353767|AW000353629|AW000353619|AW000353559|AW000353399|AW000353369|AW000353339|AW000353333|AW000353299|AW000353297|AW000353281|AW000353262|AW000353237|AW000353061|AW000352969|AW000352951|AW000352872|AW000352821|AW000352801|AW000352742|AW000352675|AW000352632|AW000352605|AW000352414|AW000352331|AW000352214|AW000352194|AW000352184|AW000352099|AW000352074|AW000351869|AW000351839|AW000351795|AW000351613|AW000351550|AW000351349|AW000351308|AW000351256|AW000351232|AW000351043|AW000351016|AW000350954|AW000350815|AW000350741|AW000350517|AW000345080|AW000353081|AW000350146|AW000354503|AW000352765|AW000352492|AW000352616|AW000351589|AW000352100|AW000352447|AW000354445"

# set the date range to search through the tracking files.
$StartDate = "20180410"
$EndDate = "20180426"

$RemoteComputer = "wndirect-prod-ct.wndCloud.local"

$RemoteScrDir = "C:\WnControlTower\Dropbox\Output\Amazon\Archive"
$RemoteBackup = "H:\Backups\Archive temp"

# copy all tracking files to local PC as it will be quicker to run on server.


$LocalDir = "C:\temp\Amazon"
$LocalSrcDir = $LocalDir + "\" + $StartDate + "-" + $EndDate


# if folder does not exist then create it
if(!(Test-Path $LocalDir)){
    New-Item -ItemType directory -Path $LocalDir} 

# if folder does not exist then create it
if(!(Test-Path $LocalSrcDir)){
    New-Item -ItemType directory -Path $LocalSrcDir} 





if($heavyLoad -eq 1){ # from local PC scans CT server for files and then copies them to local PC
   
    
    Write-Output "Discovering Tracking Files"                                   
    (measure-command {$FileList = Get-ChildItem ("\\"+$RemoteComputer+"\"+$RemoteScrDir.replace(":","$")) -File -Filter *.xml | Where-Object {($_.CreationTime.ToString('yyyyMMdd') -ge $StartDate -and $_.CreationTime.ToString('yyyyMMdd')  -le $EndDate)}   }).TotalSeconds 
                      $i = 1                                     
                      $FileList                                      
                      write-host ""
                      (Measure-Command {foreach ($file in $FileList){
                        write-host "Copying " $i "out of" $FileList.count  " "  $File.FullName                                     
                        Copy-Item $File.Fullname $LocalSrcDir
                        $i++ }}).TotalSeconds 
                } 

 Elseif ($heavyLoad -eq 0) { # runs commands on CT to find files and then zip them.
   Invoke-Command –ComputerName $RemoteComputer –ScriptBlock { param($StartDate,$EndDate,$FileList ,$RemoteScrDir, $RemoteBackup)
                                                            Write-Output "Finding Tracking Files"
                                                           
                                                            (measure-command {$FileList = Get-ChildItem $RemoteScrDir -File -Filter *.xml | Where-Object {($_.CreationTime.ToString('yyyyMMdd') -ge $StartDate -and $_.CreationTime.ToString('yyyyMMdd')  -le $EndDate)} }).TotalSeconds 
                                                            $FileList
                                                            Write-Output "Creating Zip file containing Tracking files"
                                                            (measure-command {$FileList | compress-Archive  -CompressionLevel Fastest -DestinationPath $RemoteBackup"\TestAmazon" -Force}).TotalSeconds
                                                            
                                                           } -ArgumentList $StartDate,$EndDate, $FileList ,$RemoteScrDir, $RemoteBackup



Write-Output "Copying Zip file to local PC"
Move-item ("\\"+$RemoteComputer+"\"+$RemoteBackup.replace(":","$")+"\TestAmazon.zip") $LocalDir -Force



Write-Output "Extracting files"
Expand-Archive -LiteralPath $LocalDir"\TestAmazon.zip" -DestinationPath $LocalSrcDir -Force

}



# create a folder called found, the results file will be placed here
$Found = $LocalSrcDir + '\' + 'Found'

# path to where the Amazon tracking files are stored on Control Tower
$PCCTpath = "\\10.129.215.34\Amazon Tracking\"

$MacCTpath = "smb://10.129.215.34/Amazon Tracking/"

    
# create a list of all files in folder
$filesInDir = Get-ChildItem $LocalSrcDir -file -Filter *.xml

$i= 1

# if folder does not exist then create it
if(!(Test-Path $Found)){
    New-Item -ItemType directory -Path $Found} 

# create a blank object
$results = @()

# check each file in folder
(Measure-Command {foreach ($file in $filesInDir){
    
        write-host "reading " $i "out of" $filesInDir.count  " "  $file.FullName 
        # read each file into memory
        [xml]$xml = Get-Content $file.FullName
        # all useful info is stored under ShipmentInformation/ShipmentStatusSeq/ShipmentStatus
        $f = $xml.SelectNodes("ShipmentInformation/ShipmentStatusSeq/ShipmentStatus")
        
        # check each node in the XML object
        foreach ($items in $f){
            # Find parcel 
            if ($items.ShipmentIdentification.CarrierTrackingNum -imatch $parcels){
          
             # once found parcel add details to an object
             $details = [Ordered]@{            
                Parcel           = $items.ShipmentIdentification.CarrierTrackingNum            
                Status           = $items.ShipmentStatusInformation.Status                 
                StatusReason     = $items.ShipmentStatusInformation.StatusReason
                CountryCode          = $items.ShipToInformation.Address.CountryCode 
                Date             = $file.lastWriteTime.ToString('yyyy-MM-dd')
#                PCFilename       = "=HYPERLINK("+ [char]34 + $PCCTpath + $file.Name.ToString() + [char]34 + ","+ [char]34 + $file.Name.ToString() + [char]34 + ")"
#                MacFilename       = "=HYPERLINK("+ [char]34 + $MacCTpath + $file.Name.ToString() + [char]34 + ","+ [char]34 + $file.Name.ToString() + [char]34 + ")"
                PCFilename       = "=HYPERLINK("+ [char]34 + $PCCTpath + $file.Name.ToString() + [char]34 + ","+ [char]34 + $file.Name.ToString() + [char]34 + ")"
               MacFilename       = "=HYPERLINK("+ [char]34 + $MacCTpath + $file.Name.ToString() + [char]34 + ","+ [char]34 + $file.Name.ToString() + [char]34 + ")"
               FileLengthInKB        = $file.Length / 1KB
             }                           
         # repeat adding found parcels to object    
        $results += New-Object PSObject -Property $details  
        }
        }
        $i++          
     }
}).TotalSeconds

$a = get-date
#$a.tostring("hh.mmdd-MM-yyyy-")

# export results object to csv file.
(Measure-Command{ $results | export-csv -Path ($Found + '\' + 'NickAmazon'+ ($a.tostring("hh.mm-dd-MM-yyyy-")) + 'foundtracking.csv') -NoTypeInformation -Append   }).Totalseconds    
        


